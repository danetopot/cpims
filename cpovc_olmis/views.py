from django.shortcuts import render
from cpovc_olmis.forms import (APISearchForm)

# Create your views here.
def pull_data(request):
    form = APISearchForm()
    return render(request, 'rest_framework/pull_data.html', { 'form': form })


def push_data(request):
    return render(request, 'rest_framework/push_data.html')
