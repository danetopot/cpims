from django import forms
from django.utils.translation import ugettext_lazy as _
from cpovc_main.functions import get_list

person_type_list = get_list('person_type_id', 'Please Select')
org_unit_type_list = get_list('organisation_type_id', 'Please Select')


class APISearchForm(forms.Form):
    person_type = forms.ChoiceField(choices=person_type_list,
                                    initial='0',
                                    required=True,
                                    widget=forms.Select(
                                        attrs={'class': 'form-control',
                                               'id': 'person_type'})
                                    )

    person_id = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Person ID'),
               'class': 'form-control',
               'id': 'person_id'}))

    # -----------------------------------------------------------------#
    org_unit_type = forms.ChoiceField(choices=org_unit_type_list,
                                    initial='0',
                                    required=True,
                                    widget=forms.Select(
                                        attrs={'class': 'form-control',
                                               'id': 'org_unit_type'})
                                    )

    org_unit_id = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter OrgUnit ID'),
               'class': 'form-control',
               'id': 'org_unit_id'}))
