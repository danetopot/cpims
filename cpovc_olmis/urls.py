from django.conf.urls import patterns, url

# This should contain urls related to registry ONLY
urlpatterns = patterns(
    # Forms Registry
    'cpovc_olmis.views',
    url(r'^push_data/$', 'push_data', name='push_data'),
    url(r'^pull_data/$', 'pull_data', name='pull_data'),
)
