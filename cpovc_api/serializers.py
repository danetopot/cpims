"""Serializers for the test API."""
from cpovc_auth.models import AppUser
from cpovc_registry.models import RegOrgUnit, RegPerson, RegPersonsTypes
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """User serializer."""

    class Meta:
        """Overrride parameters."""

        model = AppUser
        fields = ('first_name', 'surname', 'id')


class OrgUnitSerializer(serializers.HyperlinkedModelSerializer):
    # Organisation Unit serializer.
    class Meta:
        """Overrride parameters."""

        model = RegOrgUnit
        fields = ('org_unit_id_vis', 'org_unit_name', 'org_unit_type_id', 'parent_org_unit_id', 'id')
        read_only_fields = ('org_unit_id_vis', 'org_unit_name')


class PersonTypeSerializer(serializers.HyperlinkedModelSerializer):
    # RegPersonsTypes serializer.
    class Meta:
        """Overrride parameters."""

        model = RegPersonsTypes
        fields = ('person_type_id')


class PersonsSerializer(serializers.ModelSerializer):
    class Meta:
    	model = RegPerson
    	fields = ('id', 'designation','first_name', 'other_names','surname','email','des_phone_number','date_of_birth','date_of_death', 'is_void')

    def to_representation(self, obj):
    	persontypes = []
    	serialized_data = super(PersonsSerializer, self).to_representation(obj) # original serialized data
    	person_id = serialized_data.get('id') # get person id from original serialized data
    	person_types = RegPersonsTypes.objects.filter(person_id=person_id) # get the object from db
    	
    	if person_types:
    		for person_type in person_types:
    			persontypes.append(person_type.person_type_id)

    	serialized_data['person_type'] = ', '.join(persontypes) # replace id with name
    	return serialized_data # return modified serialized data