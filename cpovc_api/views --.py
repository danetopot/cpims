"""Test API for users."""
from rest_framework import viewsets
from .serializers import UserSerializer, OrgUnitSerializer, PersonSerializer
from django.http.response import Response
from cpovc_auth.models import AppUser
from cpovc_registry.models import RegOrgUnit, RegPerson


class UserViewSet(viewsets.ModelViewSet):
    """API endpoint that allows users to be viewed or edited."""

    queryset = AppUser.objects.all().order_by('-last_login')
    serializer_class = UserSerializer


class OrgUnitsViewSet(viewsets.ReadOnlyModelViewSet):
    """API endpoint that allows Organisation Units to be viewed or edited."""

    queryset = RegOrgUnit.objects.filter(
        is_void=False).order_by('org_unit_name')
    serializer_class = OrgUnitSerializer


class PersonViewSet(viewsets.ModelViewSet):
    """

    API endpoint that allows Persons to be viewed or edited.

    NOTES
    -----

    This viewset automatically provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
    Additionally we also provide an extra `highlight` action.

    """

    # This code saves you from repeating yourself
    queryset = RegPerson.objects.all()
    serializer_class = PersonSerializer

    def list(self, request, *args, **kwargs):

    	# Get your variables from request
   		person_id = request.QUERY_PARAMS.get(
   		    'person_id', None)  # for POST requests

   		if person_id:
	   		data = self.get_queryset().filter(person_id)
	        serialized_data = self.get_serializer(data, many=True)
	    	return Response(serialized_data.data)

	    # Default behaviour : call parent
    	return super(PersonViewSet, self).list(request, *args, **kwargs)


    
    
