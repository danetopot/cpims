"""Test API for users."""
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.contrib import messages
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, OrgUnitSerializer, PersonsSerializer, PersonTypeSerializer
from cpovc_auth.models import AppUser
from cpovc_registry.models import RegOrgUnit, RegPerson, RegPersonsTypes
from cpovc_registry.functions import(save_person_type, save_locations, save_external_ids, save_geo_location, save_contacts, geos_from_aids, org_id_generator)
from cpovc_main.functions import convert_date

class UserViewSet(viewsets.ModelViewSet):
    """API endpoint that allows users to be viewed or edited."""

    queryset = AppUser.objects.all().order_by('-last_login')
    serializer_class = UserSerializer


class OrgUnitsViewSet(viewsets.ModelViewSet):
    # This code saves you from repeating yourself
    queryset = RegOrgUnit.objects.all()
    serializer_class = OrgUnitSerializer

    def list(self, request, *args, **kwargs):
        org_unit_id = request.query_params.get('org_unit_id', None)
        org_unit_type = request.query_params.get('org_unit_type', None)

        if org_unit_id and not org_unit_type:
            # Get your data according to the variable org_unit_id
            data = self.get_queryset().filter(pk=org_unit_id)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        elif org_unit_type and not org_unit_id:
            # Get your data according to the variable org_unit_type
            data = self.get_queryset().filter(org_unit_type_id=org_unit_type)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        elif org_unit_type and org_unit_id:
            # Get your data according to the variable org_unit_type and org_unit_id
            data = self.get_queryset().filter(pk=org_unit_id, org_unit_type_id=org_unit_type)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        else:
            # Default behaviour : call parent
            return super(OrgUnitsViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        # get pushed data QueryDict
        data=request.data
        print 'API - orgunits data :  %s' % data

        # get/extract data
        org_unit_id_vis = data['org_unit_id_vis'] if data['org_unit_id_vis'] else None
        org_unit_name = data['org_unit_name'] if data['org_unit_name'] else None
        org_unit_type = data['org_unit_type_id'] if data['org_unit_type_id'] else None
        date_operational = data['date_operational'] if data['date_operational'] else None
        if date_operational:
            date_operational = convert_date(date_operational)
        parent_org_unit = data['parent_org_unit_id'] if data['parent_org_unit_id'] else None
        legal_reg_number = data['legal_reg_number'] if data['legal_reg_number'] else None
        org_reg_type = data['org_reg_type'] if data['org_reg_type'] else None
        county = data['county'] if data['county'] else None
        sub_county = data['sub_county'] if data['sub_county'] else None
        ward = data['ward'] if data['ward'] else None       
        source = data['source'] if data['source'] else None 
        action = data['action'] if data['action'] else None
        contact_detail_type_id = data['contact_detail_type_id'] if data['contact_detail_type_id'] else None
        contact_value = data['contact_value'] if data['contact_value'] else None

        org_new = RegOrgUnit(org_unit_id_vis='NXXXXXX',
                                 org_unit_name=org_unit_name.upper(),
                                 org_unit_type_id=org_unit_type,
                                 date_operational=date_operational,
                                 parent_org_unit_id=parent_org_unit,
                                 created_by_id=request.user.id,
                                 is_void=False)
        org_new.save()
        org_unit_id = org_new.pk
        org_unit_id_vis = org_id_generator(org_unit_id)
        org_new.org_unit_id_vis = org_unit_id_vis
        org_new.save(update_fields=["org_unit_id_vis"])

        # Save external ids
        if org_reg_type:
            save_external_ids(org_reg_type, legal_reg_number, org_unit_id)

        # Save Geo locations
        geo_locs = ward + sub_county
        if not geo_locs and county:
            geo_locs = geos_from_aids(county, area_type='GDIS')
        save_geo_location(geo_locs, org_unit_id)

        # Save Contacts
        save_contacts(contact_detail_type_id, contact_value, org_unit_id)
        
        jsonResponse = []
        jsonResponse.append({"status": True, "code": 200, "message": "Success"})
        return JsonResponse(jsonResponse, content_type='application/json',
                        safe=False)

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class PersonViewSet(viewsets.ModelViewSet):
    # This code saves you from repeating yourself
    queryset = RegPerson.objects.all()
    serializer_class = PersonsSerializer

    def list(self, request, *args, **kwargs):
        person_id = request.query_params.get('person_id', None)
        person_type = request.query_params.get('person_type', None)

        if person_id and not person_type:
            # Get your data according to the variable person_id
            data = self.get_queryset().filter(pk=person_id)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        elif person_type and not person_id:
            # Get your data according to the variable person_type
            data = self.get_queryset().filter(regpersonstypes__person_type_id=person_type)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        elif person_type and person_id:
            # Get your data according to the variable person_type
            data = self.get_queryset().filter(pk=person_id, regpersonstypes__person_type_id=person_type)
            serialized_data = self.get_serializer(data, many=True)
            return Response(serialized_data.data)
        else:
            # Default behaviour : call parent
            return super(PersonViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):

        # get pushed data QueryDict
        data=request.data
        print 'API - person data :  %s' % data

        # get/extract data
        designation = data['designation'] if data['designation'] else None
        first_name = data['first_name'] if data['first_name'] else None
        other_names = data['other_names'] if data['other_names'] else None
        surname = data['surname'] if data['surname'] else None
        email = data['email']
        des_phone_number = data['des_phone_number'] if data['des_phone_number'] else None
        sex_id = data['sex_id'] if data['sex_id'] else None
        date_of_birth = data['date_of_birth'] if data['date_of_birth'] else None
        if date_of_birth:
            date_of_birth = convert_date(date_of_birth)
        date_of_death = data['date_of_death'] if data['date_of_death'] else None
        if date_of_death:
            date_of_death = convert_date(date_of_death)
        living_in = data['living_in'] if data['living_in'] else None
        living_in_ward = data['living_in_ward'] if data['living_in_ward'] else None
        persontype = data['person_types'] if data['person_types'] else None 
        source = data['source'] if data['source'] else None 
        action = data['action'] if data['action'] else None 
              

        # Capture RegPerson Model
        person = RegPerson(
            designation=designation,
            first_name=first_name.upper(),
            other_names=other_names.upper(),
            surname=surname.upper(), 
            sex_id=sex_id,
            des_phone_number=des_phone_number,
            email=email, 
            date_of_birth=date_of_birth,
            date_of_death=None, 
            created_by_id=request.user.id,
            is_void=False)
        person.save()

        reg_person_pk = int(person.pk)
        now = timezone.now()

        # Capture RegPersonTypes Model
        person_types = [persontype]
        if person_types:
            save_person_type(person_types, reg_person_pk)

        # Capture RegPersonsGeo Model
        area_ids = {}
        if living_in:
            area_ids[living_in] = 'GLTL'
        if living_in_ward:
            area_ids[living_in_ward] = 'GLTL'
        save_locations(area_ids, int(reg_person_pk))

        jsonResponse = []
        jsonResponse.append({"status": True, "code": 200, "message": "Success"})
        return JsonResponse(jsonResponse, content_type='application/json',
                        safe=False)

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass

