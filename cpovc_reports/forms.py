"""Forms for Reports section of CPIMS."""
from datetime import datetime
from django import forms
from cpovc_registry.functions import (
    get_all_geo_list, get_geo_list, get_specific_orgs)

from cpovc_main.functions import get_list


all_list = get_all_geo_list()
county_list = get_geo_list(all_list, 'GPRV', 'Please Select County')
sub_county_list = get_geo_list(all_list, 'GDIS', 'Please Select Sub-county')
document_type = get_list('document_type_id', 'Select report/document type')

report_types = (('', 'Select type'), ('M', 'Monthly'),
                ('Q', 'Quarterly'), ('Y', 'Yearly'))

report_vars = (('', 'Select Variable'), (1, 'Organisation Unit'))
# ,(2, 'Work Force'), (3, 'Case category'))

report_period = ()

YEAR_CHOICES = []
for yr in range(2015, (datetime.now().year + 1)):
    yr_text = '%d/%d' % (yr, yr + 1)
    YEAR_CHOICES.append((yr, yr_text))


class CaseLoad(forms.Form):
    """Class for case load reports forms."""

    def __init__(self, user, *args, **kwargs):
        """Constructor for override especially on fly data."""
        self.user = user
        super(CaseLoad, self).__init__(*args, **kwargs)
        org_units = get_specific_orgs(self.user.reg_person_id)
        org_unit = forms.ChoiceField(
            choices=org_units,
            initial='',
            widget=forms.Select(
                attrs={'class': 'form-control',
                       'autofocus': 'true'}))
        self.fields['org_unit'] = org_unit
    '''
    sub_county = forms.MultipleChoiceField(
        choices=county_list,
        initial='',
        widget=forms.SelectMultiple(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'data-parsley-errors-container': "#county_error",
                   'id': 'sub_county'}))
    '''
    county = forms.ChoiceField(
        choices=county_list,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'data-parsley-errors-container': "#county_error",
                   'id': 'county'}))
    sub_county = forms.ChoiceField(
        choices=sub_county_list,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'data-parsley-errors-container': "#sub_county_error",
                   'id': 'sub_county'}))

    document_type = forms.ChoiceField(
        choices=document_type,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'autofocus': 'true'}))

    report_type = forms.ChoiceField(
        choices=report_types,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'autofocus': 'true'}))

    report_year = forms.ChoiceField(
        choices=YEAR_CHOICES,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'autofocus': 'true'}))

    report_period = forms.ChoiceField(
        choices=report_period,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'autofocus': 'true'}))

    child = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'data-parsley-required': 'true',
               'id': 'child_id'}))

    cpims_child = forms.CharField(widget=forms.HiddenInput(
        attrs={'id': 'cpims_child_id'}))

    report_id = forms.CharField(widget=forms.HiddenInput(
        attrs={'id': 'report_id'}))

    report_vars = forms.ChoiceField(
        choices=report_vars,
        initial='',
        widget=forms.Select(
            attrs={'class': 'form-control',
                   'data-parsley-required': 'true',
                   'autofocus': 'true'}))
