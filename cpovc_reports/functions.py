"""Reports functions."""
import re
from datetime import datetime, timedelta
from calendar import monthrange, month_name
from collections import OrderedDict

from reportlab.pdfgen import canvas
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.platypus import (
    SimpleDocTemplate, Paragraph, Spacer, Image, Frame, Table)
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch, cm, mm
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.lib.pagesizes import A4, letter

from cpovc_main.functions import get_general_list, get_dict
from cpovc_main.models import SetupGeography

from .config import reports
from cpovc_registry.functions import merge_two_dicts
from cpovc_registry.models import RegOrgUnitGeography, RegPerson, RegOrgUnit

from cpovc_forms.models import (
    OVCCaseCategory, OVCCaseGeo, OVCCaseEventServices,
    OVCAdverseMedicalEventsFollowUp, OVCPlacement,
    OVCDischargeFollowUp)

from django.conf import settings
from django.db.models import Count

MEDIA_ROOT = settings.MEDIA_ROOT
STATIC_ROOT = settings.STATICFILES_DIRS[0]


class Canvas(canvas.Canvas):
    """Pagination extention for canvas."""

    def __init__(self, *args, **kwargs):
        """Constructor for my pagination."""
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        """Get the pages first."""
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """To add page info to each page (page x of y)."""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        """Draw the final page."""
        self.setFont("Times-Roman", 7)
        self.drawRightString(20 * cm, 1 * cm,
                             "Page %d of %d" % (self._pageNumber, page_count))


def get_report_body(params, report='DSCE'):
    """Method to get report body."""
    try:
        fields = OrderedDict()
        dot_padd = '.' * 170
        dash_padd = ''
        # '_' * 70
        rparams = child_data(params)
        values = {'padd_dash': dash_padd, 'padd_dot': dot_padd, 'province': '',
                  'nationality': '', 'religion': '', 'district': '',
                  'location': '', 'sub_location': '', 'chief': '',
                  'village': ''}
        params = merge_two_dicts(values, rparams)
        report_id = report if report in reports else 'NONE'
        report_params = reports[report_id]
        report_data = report_params % params
        repos = report_data.split('\n')
        cnt = 0
        for repo_val in repos:
            field_values = repo_val.strip()
            field_vals = field_values.split(':', 1)
            field_txt = None if len(field_vals) < 2 else field_vals[1]
            if '<' in field_vals[0] and '>' in field_vals[0]:
                cnt += 1
                fields[field_vals[0].replace('>', '_%s>' % cnt)] = field_txt
            else:
                fields[field_vals[0]] = field_txt
        return fields
    except Exception, e:
        raise e


def get_case_details(field_names):
    """Method to get only case categories from list general."""
    try:
        case_categories = get_general_list(field_names)
    except Exception, e:
        print "Error getting case categories - %s" % (str(e))
        pass
    else:
        return case_categories


def case_load_header(report_type=1, header=False, ages=True):
    """Method to generate reports headings."""
    try:
        age_ranges = ['0 - 5 yrs', '6 - 10 yrs', '11 - 15 yrs',
                      '16 - 17 yrs', '18+ yrs', 'Sub-Total']
        genders = ['M', 'F']
        html = ""
        title = 'Case Category (New Cases)'
        if header:
            html = ("<tr><td colspan='3'>County</td><td colspan='6'>"
                    "{county}</td><td colspan='3'>Year</td>"
                    "<td colspan='4'>{years}</td></tr>")
            html += ("<tr><td colspan='3'>Sub County</td><td colspan='6'>"
                     "{sub_county}</td><td colspan='3'>Month</td>"
                     "<td colspan='4'>{month}</td></tr>")
            if report_type == 4:
                html = ("<tr><td colspan='3'>Organisation</td><td colspan='6'>"
                        "{org_unit_name}</td><td colspan='3'>Year</td>"
                        "<td colspan='4'>{years}</td></tr>")
                html += ("<tr><td colspan='3'>Unit</td><td colspan='6'>"
                         "{org_units_name}</td><td colspan='3'>Month</td>"
                         "<td colspan='4'>{month}</td></tr>")
            html += ("<tr><td colspan='16'>{period} Case Load Summary"
                     "</td></tr>")
        html += "<tr><td colspan='3' rowspan='2'>%s</td>" % (title)
        for age_range in age_ranges:
            html += "<td colspan='2'>%s</td>" % (age_range)
        html += "<td rowspan='2'>TOTAL</td></tr>"
        html += "<tr>"
        for age_range in age_ranges:
            for gender in genders:
                html += "<td>%s</td>" % (gender)
        html += "</tr>"
        return html
    except Exception, e:
        raise e


def get_data_element(item='case'):
    """Method to get data elements other than categories and inteventions."""
    results = {}
    try:
        case, params = {}, {}
        case[1] = 'Total Children'
        case[2] = 'Total Cases'
        case[3] = 'Total Interventions'
        case[4] = 'Percentage Interventions'
        # dropped out -  (90+ days no intervention)
        case[5] = 'Dropped Out'
        case[6] = 'Pending'

        results['case'] = case
        if item in results:
            params = results[item]
    except Exception:
        pass
    else:
        return params


def simple_documents(params, document_name='CPIMS', report_name='letter'):
    """Method to generate simple report."""
    try:
        file_name = "%s/%s.pdf" % (MEDIA_ROOT, report_name)
        doc = SimpleDocTemplate(file_name, pagesize=letter,
                                rightMargin=48, leftMargin=48,
                                topMargin=30, bottomMargin=30)
        story = []
        logo = "%s/img/gok_logo.jpg" % (STATIC_ROOT)
        today = datetime.now()
        d = int(today.strftime('%d'))
        st_rd = {1: 'st', 2: 'nd', 3: 'rd'}
        suffix = 'th' if 11 <= d <= 13 else st_rd.get(d % 10, 'th')
        # formatted_date = today.strftime('%d{S} %B %Y').replace('{S}', suffix)
        suffix = '<sup>%s</sup>' % (suffix)
        formatted_date = '{dt.day}{S} {dt:%B}, {dt.year}'.format(
            dt=today, S=suffix)

        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(
            name='Justify', alignment=TA_JUSTIFY, leading=22))

        styles.add(ParagraphStyle(name="Centered", alignment=TA_CENTER,
                                  leading=22))
        heading = 'Ministry of Labour and East African Community Affairs'
        sub_heading = 'DEPARTMENT OF CHILDREN SERVICES'
        ptext = '<font size=16><b>%s</b></font>' % heading.upper()
        story.append(Paragraph(ptext, styles["Centered"]))
        # story.append(Spacer(1, 12))
        ptext = '<font size=13><b>%s</b></font>' % sub_heading
        story.append(Paragraph(ptext, styles["Centered"]))
        story.append(Spacer(1, 12))

        sst = getSampleStyleSheet()
        tgram = '.' * 34
        ref = '.' * 36

        p0 = Paragraph('''
            <font size=9>Telegram %s <br/>
            Telephone: 020 2077160<br/>
            When replying please quote<br/><br/>
            Ref. No. %s</font>
            ''' % (tgram, ref), sst["BodyText"])

        org_unit = params['org_unit']
        address = params['address']
        p1 = Paragraph('''
               <font size=9>%s<br/>
               %s<br/><br/>
               Date. %s</font>
               ''' % (org_unit, address, formatted_date), sst["BodyText"])
        im = Image(logo, 1.2 * inch, 1.0 * inch)
        # story.append(im)

        data = [[p0, im, p1]]

        t = Table(data, style=[('ALIGN', (0, 0), (2, 0), 'CENTER'),
                               ('VALIGN', (0, 0), (2, 0), 'MIDDLE'), ])

        story.append(t)
        story.append(Spacer(1, 12))

        # Create return address
        ptext = ('<para align=center spaceb=3><font size=12>'
                 '<b>%s</b></font></para>') % document_name.upper()
        story.append(Paragraph(ptext, styles["Normal"]))
        story.append(Spacer(1, 24))
        vals = get_report_body(params, report_name)
        ptext = '%s' % (vals)
        story.append(Paragraph(ptext, styles["Justify"]))
        story.append(Spacer(1, 12))
        doc.build(story, onFirstPage=draw_page, onLaterPages=draw_page,
                  canvasmaker=Canvas)
    except Exception, e:
        raise e


def coord(x, y, unit=1):
    """Get my size."""
    width, height = A4
    x, y = x * unit, height - y * unit
    return x, y


def get_style(style=False):
    """Get the style required."""
    try:
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(
            name='Justify', alignment=TA_JUSTIFY, leading=22))
        styles.add(ParagraphStyle(
            name="Centered", alignment=TA_CENTER, leading=22))
        width, height = letter
        my_style = styles["Normal"]
        if style and style in styles:
            my_style = styles[style]
        return my_style
    except Exception:
        pass


def create_paragraph(c, text, x, y, style=False):
    """Method to do paragraphing."""
    try:
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(
            name='Justify', alignment=TA_JUSTIFY, leading=22))
        styles.add(ParagraphStyle(
            name="Centered", alignment=TA_CENTER, leading=22))
        width, height = letter
        my_style = styles["Normal"]
        if style and style in styles:
            my_style = styles[style]
        p = Paragraph(text, style=my_style)
        p.wrapOn(c, width, height)
        p.drawOn(c, *coord(x, y, mm))
    except Exception:
        pass


def date_suffix(theday):
    """Show date suffix."""
    d = int(theday.strftime('%d'))
    st_rd = {1: 'st', 2: 'nd', 3: 'rd'}
    suffix = 'th' if 11 <= d <= 13 else st_rd.get(d % 10, 'th')
    return suffix


def simple_document(params, document_name='CPIMS', report_name='letter'):
    """Method to write forms."""
    from reportlab.pdfgen import canvas
    try:
        file_name = "%s/%s.pdf" % (MEDIA_ROOT, report_name)
        width, height = A4
        sst = getSampleStyleSheet()
        tgram = '.' * 34
        ref = '.' * 36

        logo = "%s/img/gok_logo.jpg" % (STATIC_ROOT)
        today = datetime.now()
        dt_suffix = date_suffix(today)
        suffix = '<sup>%s</sup>' % (dt_suffix)
        formatted_date = '{dt.day}{S} {dt:%B}, {dt.year}'.format(
            dt=today, S=suffix)

        p0 = Paragraph('''
            <font size=9>Telegram %s <br/>
            Telephone: 020 2077160<br/>
            When replying please quote<br/><br/>
            Ref. No. %s</font>
            ''' % (tgram, ref), sst["BodyText"])

        org_unit = params['org_unit']
        address = params['address']
        p1 = Paragraph('''
               <font size=9>%s<br/>
               %s<br/><br/>
               Date. %s</font>
               ''' % (org_unit, address, formatted_date), sst["BodyText"])
        im = Image(logo, 1.2 * inch, 1.0 * inch)
        data = [[p0, im, p1]]

        t = Table(data, style=[('ALIGN', (0, 0), (2, 0), 'CENTER'),
                               ('VALIGN', (0, 0), (2, 0), 'MIDDLE'), ])

        canvas = canvas.Canvas(file_name, pagesize=letter)
        fontname, fontsize = 'Times-Roman', 12
        # print canvas.getAvailableFonts()
        canvas.setLineWidth(.4)
        canvas.setFont(fontname, fontsize)
        # print canvas.getAvailableFonts()
        story = []
        normal_style = get_style("Normal")
        center_style = get_style("Centered")
        heading = 'Ministry of Labour and East African Community Affairs'
        sub_heading = 'DEPARTMENT OF CHILDREN SERVICES'
        ptext = '<font size=14><b>%s</b></font>' % heading.upper()
        story.append(Paragraph(ptext, center_style))
        ptext = '<font size=13><b>%s</b></font>' % sub_heading
        story.append(Paragraph(ptext, center_style))
        story.append(Spacer(1, 12))
        story.append(t)
        story.append(Spacer(1, 12))

        # Create return address
        ptext = ('<para align=center spaceb=3><font size=12>'
                 '<b>%s</b></font></para>') % document_name.upper()
        story.append(Paragraph(ptext, normal_style))
        # story.append(Spacer(1, 24))

        f = Frame(inch, inch, 6.5 * inch, 9.4 * inch, showBoundary=0)
        f.addFromList(story, canvas)

        # create_paragraph(canvas, ptext, 10, 48, 'Centered')
        fields = get_report_body(params, report=report_name)
        row_num = height - 280
        for field in fields:
            s_field = ''
            field_value = fields[field]
            myfont = 'Times-Bold' if field.isupper() else 'Times-Roman'
            canvas.setFont(myfont, 12)
            twidth = stringWidth(field, fontname, fontsize)
            if field_value or field_value == '':
                fd = unicode(str(field), 'utf-8')
                s_field = '.' if fd.isnumeric() else ':'
                if '<line' in field:
                    twidth = -20
                canvas.line(70 + twidth, row_num, width - 40, row_num)
                canvas.drawString(70 + twidth, row_num + 3, field_value)
            fid = s_field if '<blank' in field else '%s%s' % (field, s_field)
            if '<line' not in field:
                if field.startswith('<title_'):
                    ttext = re.sub(r'(?i)^<title_+\d+>', '', fid)
                    canvas.drawCentredString(width / 2, row_num + 3, ttext)
                else:
                    canvas.drawString(50, row_num + 3, fid)
            if row_num <= 80:
                canvas.showPage()
                canvas.setLineWidth(.4)
                canvas.setFont(myfont, 12)
                row_num = height - 100
            row_num -= 20

        canvas.save()
    except Exception, e:
        print 'error - %s' % (str(e))
        pass


def word_document(params, name):
    """Method to write to word - .docx."""
    try:
        from docx import Document
        from docx.shared import Inches

        document = Document()

        logo = "%s/img/gok_logo.jpg" % (STATIC_ROOT)

        document.add_heading('Document Title', level=0)

        p = document.add_paragraph('A plain paragraph having some ')
        p.add_run('bold').bold = True
        p.add_run(' and some ')
        p.add_run('italic.').italic = True

        document.add_paragraph(
            'first item in ordered list', style='List Number'
        )

        document.add_picture(logo, width=Inches(1.1))

        table = document.add_table(rows=1, cols=3)
        table.rows[0].style = "border:0;"
        hdr_cells = table.rows[0].cells
        hdr_cells[0].text = 'Qty'
        hdr_cells[1].text = 'Id'
        hdr_cells[2].text = 'Desc'

        document.add_page_break()
        document.save('demo.docx')

    except Exception, e:
        raise e


def draw_page(canvas, doc):
    """Method to format my pdfs."""
    title = "CPIMS"
    author = "CPIMS"
    canvas.setTitle(title)
    canvas.setSubject(title)
    canvas.setAuthor(author)
    canvas.setCreator(author)

    footer = []
    # Put some data into the footer
    Frame(2 * cm, 0, 17 * cm, 4 * cm).addFromList(footer, canvas)


def get_sub_county_info(sub_county_ids, a_type='GDIS', icounty=None):
    """Get selected sub-counties info and attached counties."""
    area_ids, counties = {}, {}
    try:
        county_set = SetupGeography.objects.filter(
            area_type_id='GPRV', is_void=False)
        for county in county_set:
            counties[county.area_id] = county.area_name
        if icounty:
            sub_county_ids = SetupGeography.objects.filter(
                parent_area_id=icounty, area_type_id='GDIS',
                is_void=False).values_list('area_id', flat=True)
        if sub_county_ids:
            areas = SetupGeography.objects.filter(
                area_id__in=sub_county_ids, is_void=False)
            for area in areas:
                area_id = area.area_id
                area_type = area.area_type_id
                area_name = area.area_name
                parent_area_id = area.parent_area_id
                if parent_area_id in counties:
                    parent_area_id = counties[parent_area_id]
                if area_type == a_type:
                    area_ids[area_id] = {'county': parent_area_id,
                                         'sub_county_id': area_id,
                                         'sub_county': area_name}
    except Exception, e:
        print 'error getting sub-county ids - %s' % (str(e))
        raise e
    else:
        return area_ids


def get_geo_locations(org_id, a_type='GDIS'):
    """Get specific Organisational units location based on org id."""
    ext_ids = []
    try:
        areas = RegOrgUnitGeography.objects.select_related().filter(
            org_unit_id=org_id, is_void=False)
        for area in areas:
            area_type = area.area.area_type_id
            area_name = area.area.area_name
            if area_type == a_type:
                ext_ids.append(area_name)
    except Exception, e:
        raise e
    else:
        return ext_ids


def get_period(report_type='M', month='', year='', period='F'):
    """
    Method to generate date ranges in preparation for the query.

    The report types include M, Q1, Q2, Q3 and Y
    period should be a calculated month range given an end date.
    """
    try:
        days = 30
        reports_qs = {'Q1': 9, 'Q2': 12, 'Q3': 3, 'Q4': 6}
        other_yr = ['Q3', 'Q4', 'Y']
        yr_add = 1 if report_type in other_yr else 0
        if period == 'C':
            yr_add = 0
            reports_qs = {'Q1': 3, 'Q2': 6, 'Q3': 9, 'Q4': 12}
        if not month and not year:
            today = datetime.now()
            year = today.strftime('%Y')
            month = today.strftime('%m')
        if report_type in reports_qs:
            days = 89 if report_type == 'Q3' else 90
            if period == 'C':
                days = 89 if report_type == 'Q1' else 90
            month = reports_qs[report_type]
        elif report_type == 'Y':
            # Days in year intentionally made < 365.25 by a day
            # for using timedelta replace day=1
            days = 364
            month = 12 if period == 'C' else 6
        year = int(year) + yr_add
        start_day_week, end_day = monthrange(int(year), int(month))
        end_date = '%s-%s-%s' % (end_day, month, year)
        end_date_obj = datetime.strptime(end_date, '%d-%m-%Y')
        # print (end_date_obj - timedelta(days=days))
        start_date_obj = (end_date_obj - timedelta(days=days)).replace(day=1)
        params = {}
        params['end_date'] = end_date_obj
        params['start_date'] = start_date_obj
        # Period name for the report
        report_name = 'Quarterly' if report_type in reports_qs else 'Monthly'
        report_name = 'Annual' if report_type == 'Y' else report_name
        # Months names for the report
        mon_name = month_name[int(month)]
        sheet_name = '%s-%s' % (mon_name[:3], (str(year))[-2:])
        if report_type in reports_qs:
            s_mon = month_name[reports_qs[report_type] - 2][:3]
            mon_name = '%s-%s' % (s_mon, mon_name[:3])
            sheet_name = report_type.replace('Q', 'Qtr')
        mon_name = 'Jul-Jun' if report_type == 'Y' else mon_name
        if period == 'C':
            mon_name = 'Jan-Dec' if report_type == 'Y' else mon_name
        sheet_name = 'YEAR' if report_type == 'Y' else sheet_name
        # Start year
        s_year = year - yr_add
        year_name = '%s/%s' % (s_year, year) if report_type == 'Y' else year
        # Report label
        report_label = mon_name if report_type == 'M' else sheet_name
        params['period'] = report_name
        params['month'] = mon_name
        params['year'] = year
        params['years'] = year_name
        params['sheet'] = sheet_name
        params['label'] = report_label
        return params
    except Exception, e:
        raise e


def age_data(age, sex, vals, cat, summ=False):
    """Age set data - Upper value using range so its x-1."""
    ranges = [(0, 6), (6, 11), (11, 16), (16, 18)]
    for i, (lval, uval) in enumerate(ranges):
        if (lval <= age <= uval):
            val = i + 1
    if age >= 18:
        val = 5
    age_set = 'age_%d_%s' % (val, sex[1].lower())
    if summ:
        val = vals[cat][age_set]
        val.append(str(summ))
    else:
        val = vals[cat][age_set] + 1
    return age_set, val


def initial_values(cat, val, age, sex, intv=False):
    """Assign initial values - zeros."""
    age_sets = OrderedDict()
    for i in range(1, 6):
        age_sets['age_%s_m' % (i)] = 0
        age_sets['age_%s_f' % (i)] = 0
    if intv:
        cat = '%s_%s' % (intv, cat)
    val[cat] = age_sets
    age_set, dt = age_data(age, sex, val, cat)
    val[cat][age_set] = dt
    if intv:
        val[cat]['cat'] = intv


def sum_values(summary, cat, val, age, sex):
    """Get values."""
    age_sets = OrderedDict()
    for i in range(1, 6):
        age_sets['age_%s_m' % (i)] = []
        age_sets['age_%s_f' % (i)] = []
    val[summary] = age_sets
    age_set, dt = age_data(age, sex, val, summary, cat)
    val[summary][age_set] = dt


def data_from_results(datas, gid='D', intv=False):
    """Method to generate data set from result set.

    This has cat, sex, age
    """
    try:
        val, itv = {}, False
        for data in datas:
            cat = str(data['cat'])
            if intv:
                itv = cat
                cat = str(data['itv'])
            cid = str(data['cid'])
            kid = data['kid']
            age = data['age']
            sex = data['sex']
            if cat not in val:
                initial_values(cat, val, age, sex, itv)
            else:
                age_set, dt = age_data(age, sex, val, cat)
                val[cat][age_set] = dt
            # Summary values for unique cases and children
            if 'CASE' not in val:
                sum_values('CASE', cid, val, age, sex)
            else:
                age_set, dt = age_data(age, sex, val, 'CASE', cid)
                val['CASE'][age_set] = dt
            if gid == 'D':
                if 'CHILD' not in val:
                    sum_values('CHILD', kid, val, age, sex)
                else:
                    age_set, dt = age_data(age, sex, val, 'CHILD', kid)
                    val['CHILD'][age_set] = dt
        return val

    except Exception, e:
        raise e


def filter_org_unit(params, sub_county_case_ids):
    """Method to filter by org unit."""
    try:
        if 'org_unit' in params:
            org_unit = params['org_unit']
            if org_unit:
                org_unit_id = int(org_unit)
                sub_county_case_ids = sub_county_case_ids.filter(
                    report_orgunit_id=org_unit_id)
    except Exception:
        return sub_county_case_ids
    else:
        return sub_county_case_ids


def get_data(params, report='CASE_LOAD'):
    """
    Method to do actual query from the db.

    For now lets do case data only.
    """
    try:
        data = []
        print 'N params', params
        cl_queryset = OVCCaseCategory.objects.all()
        cl_queryset = cl_queryset.filter(is_void=False).exclude(
            case_id__is_void=True)
        # Date ranges
        org_unit = params['org_unit'] if 'org_unit' in params else False
        sc = params['sub_county_id'] if 'sub_county_id' in params else False
        # current_issue__isnull=True,
        if 'start_date' in params and 'end_date' in params:
            start_date = params['start_date']
            end_date = params['end_date']
            cl_queryset = cl_queryset.filter(
                date_of_event__range=(start_date, end_date))
        if sc:
            sub_county = params['sub_county_id']
            # Get all case ids for this sub_county
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                report_subcounty_id__in=sub_county,
                is_void=False).values_list('case_id_id', flat=True)
        else:
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                is_void=False).values_list('case_id_id', flat=True)
            if org_unit:
                sub_county_case_ids = filter_org_unit(params,
                                                      sub_county_case_ids)
        cl_queryset = cl_queryset.filter(
            case_id_id__in=(sub_county_case_ids))

        for cl in cl_queryset:
            item = {}
            item['cat'] = cl.case_category
            item['sex'] = cl.person.sex_id
            item['age'] = cl.person.years
            # For generating summaries
            item['kid'] = cl.person.id
            item['cid'] = cl.case_id_id
            data.append(item)
        pending = get_pending(params)
        interventions = get_intervention(params, pending)
        raw_data = data_from_results(data)
        raw_pending = data_from_results(pending, gid='P')
        raw_interven = data_from_results(interventions, gid='I')
        intvs = data_from_results(interventions, gid='I', intv=True)
        raw_vals = {'data': raw_data, 'pending': raw_pending,
                    'interventions': raw_interven, 'itv': intvs}
        return raw_vals
    except Exception, e:
        print 'Get data error - %s' % (str(e))
        raise e


def data_category(data):
    """Method to show my category."""
    try:
        pass
    except Exception, e:
        raise e


def get_pending(params):
    """To get all pending within the same reporting period."""
    try:
        data = []
        pen_queryset = OVCCaseCategory.objects.all()
        pen_queryset = pen_queryset.filter(is_void=False)
        # Has interventions
        cats_itvs = OVCCaseEventServices.objects.filter(
            is_void=False).values_list('case_category_id', flat=True)
        pen_queryset = pen_queryset.exclude(case_category_id__in=cats_itvs)
        # Date ranges
        org_unit = params['org_unit'] if 'org_unit' in params else False
        sc = params['sub_county_id'] if 'sub_county_id' in params else False
        # current_issue__isnull=True,
        if 'start_date' in params and 'end_date' in params:
            start_date = params['start_date']
            end_date = params['end_date']
            pen_queryset = pen_queryset.filter(
                date_of_event__range=(start_date, end_date))
        # Missing filters
        if sc:
            sub_county = params['sub_county_id']
            # Get all case ids for this sub_county
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                report_subcounty_id__in=sub_county,
                is_void=False).values_list('case_id_id', flat=True)
        else:
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                is_void=False).values_list('case_id_id', flat=True)
            if org_unit:
                sub_county_case_ids = filter_org_unit(params,
                                                      sub_county_case_ids)
        pen_queryset = pen_queryset.filter(
            case_id_id__in=(sub_county_case_ids))
        for pen in pen_queryset:
            item = {}
            item['cat'] = pen.case_category
            item['sex'] = pen.person.sex_id
            item['age'] = pen.person.years
            # For generating summaries
            item['kid'] = pen.person.id
            item['cid'] = pen.case_id_id
            data.append(item)
        return data
    except Exception, e:
        print 'Get pending error - %s' % (str(e))
        raise e


def get_intervention(params, pending=False):
    """To get all interventions within the same reporting period."""
    try:
        data = []
        itv_queryset = OVCCaseEventServices.objects.all()
        itv_queryset = itv_queryset.filter(is_void=False)
        # Start filtering , date_of_encounter_event__range
        org_unit = params['org_unit'] if 'org_unit' in params else False
        sc = params['sub_county_id'] if 'sub_county_id' in params else False
        if pending:
            pending_list = []
            for vals in pending:
                pending_list.append(vals['cid'])
            itv_queryset = itv_queryset.exclude(
                case_category__case_id_id__in=pending_list)
        if 'start_date' in params and 'end_date' in params:
            start_date = params['start_date']
            end_date = params['end_date']
            itv_queryset = itv_queryset.filter(
                date_of_encounter_event__range=(start_date, end_date))
        # Missing filters
        if sc:
            sub_county = params['sub_county_id']
            # Get all case ids for this sub_county
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                report_subcounty_id__in=sub_county,
                is_void=False).values_list('case_id_id', flat=True)
        else:
            sub_county_case_ids = OVCCaseGeo.objects.filter(
                is_void=False).values_list('case_id_id', flat=True)
            if org_unit:
                sub_county_case_ids = filter_org_unit(params,
                                                      sub_county_case_ids)
        itv_queryset = itv_queryset.filter(
            case_category__case_id__in=sub_county_case_ids)
        for res in itv_queryset:
            item = {}
            item['itv'] = res.service_provided
            item['cat'] = res.case_category.case_category
            item['kid'] = res.case_category.person_id
            item['age'] = res.case_category.person.years
            item['sex'] = res.case_category.person.sex_id
            item['cid'] = res.case_category.case_id_id
            data.append(item)
        return data
    except Exception, e:
        print 'Get intervention error - %s' % (str(e))
        raise e


def child_data(data):
    """Method to get child data from id."""
    try:
        child_id = data['child_id']
        child_info = RegPerson.objects.get(pk=child_id)
        data['name'] = str(child_info.full_name)
        data['age'] = child_info.age
        return data
    except Exception, e:
        print 'Error getting child - %s' % (str(e))
        return data


def get_raw_data(params, data_type=1):
    """Method to filter which report user wants."""
    data = None
    try:
        report_type = int(params['report_id'])
        if report_type == 5:
            data = get_raw_values(params)
        elif report_type == 4:
            vals = get_dict(field_name=['new_condition_id'])
            dt = '<table class="table table-bordered"><thead>'
            dt += '<tr><th width="40%">Disease</th>'
            dt += '<th>TOTAL</th></tr></thead>'
            total_h = 0
            diss_vals = {}
            disss = OVCAdverseMedicalEventsFollowUp.objects.values(
                'adverse_medical_condition').annotate(
                unit_count=Count('adverse_medical_condition'))
            for diss in disss:
                cond_id = diss['adverse_medical_condition']
                diss_vals[cond_id] = diss['unit_count']
            for val in vals:
                val_name = vals[val]
                val_data = diss_vals[val] if val in diss_vals else 0
                total_h += val_data
                dt += '<tr><td>%s</td>' % (val_name)
                dt += '<td>%s</td></tr>' % (0)
            dt += '<tr><td>Total</td><td>%s</td></tr>' % (total_h)
            dt += '<table>'
            data = dt
        elif report_type == 1:
            knbs_vals = {}
            knbs = OVCCaseCategory.objects.values(
                'case_category').annotate(
                unit_count=Count('case_category'))
            for knb in knbs:
                knbs_vals[knb['case_category']] = knb['unit_count']
            ids = {}
            ids['CSAB'] = 'Child offender'
            ids['CCIP'] = 'Children on the streets'
            ids['CSIC'] = 'Neglect'
            ids['CIDC'] = 'Orphaned Child'
            ids['CCIP'] = 'Children on the streets'
            ids['CDIS'] = 'Abandoned'
            ids['CHCP'] = 'Lost and found children'
            ids['CSDS'] = 'Drug & Substance Abuse'
            ids['CSNG'] = 'Physical abuse/violence'
            ids['CDSA'] = 'Abduction'
            ids['CCDF'] = 'Defilement'
            ids['CTRF'] = 'Child Labour'
            dt = '<table class="table table-bordered"><thead>'
            dt += '<tr><th width="40%">Situation of children</th>'
            dt += '<th>TOTAL</th></tr>'
            dt += '</thead>'
            knb_total = 0
            for val in ids:
                val_name = ids[val]
                val_data = knbs_vals[val] if val in knbs_vals else 0
                dt += '<tr><td>%s</td>' % (val_name)
                dt += '<td>%s</td></tr>' % (val_data)
                knb_total += val_data
            dt += '<tr><td>Total</td><td>%s</td></tr>' % (knb_total)
            dt += '<table>'
            data = dt
        elif report_type == 3:
            si_pops, si_exits = {}, {}
            spops = OVCPlacement.objects.values(
                'admission_type').annotate(
                unit_count=Count('admission_type'))
            for spop in spops:
                si_pops[spop['admission_type']] = spop['unit_count']
            # Exits
            sexits = OVCDischargeFollowUp.objects.values(
                'type_of_discharge').annotate(
                unit_count=Count('type_of_discharge'))
            for sexit in sexits:
                si_exits[sexit['type_of_discharge']] = sexit['unit_count']
            dvals = {2: 'TANA', 4: 'TARR', 6: 'TARE'}
            evals = {9: 'TDER', 10: 'TDTR', 11: 'TDEX', 12: 'DTSI'}
            ids = {1: 'Total Children by End of Previous Month',
                   2: 'New Admissions',
                   3: 'Returnees',
                   4: ' - Relapse',
                   5: ' - Recidivism',
                   6: ' - Return after escape',
                   7: 'Exits',
                   8: ' - Escapee',
                   9: ' - Early Release',
                   10: ' - Released on License',
                   11: ' - Released on Expiry of Order',
                   12: ' - Transfer to another Institution',
                   13: 'Death'}
            dt = '<table class="table table-bordered"><thead>'
            dt += '<tr><th width="40%">Situation of children</th>'
            dt += '<th>TOTAL</th></tr>'
            dt += '</thead>'
            si_total = 0
            for val in ids:
                val_name = ids[val]
                val_data = 0
                if val in dvals:
                    vd = dvals[val]
                    val_data = si_pops[vd] if vd in si_pops else 0
                if val in evals:
                    vx = evals[val]
                    val_dat = si_exits[vx] if vx in si_exits else 0
                    val_data = val_dat * -1
                val_show = val_data if val_data >= 0 else val_data * -1
                dt += '<tr><td>%s</td>' % (val_name)
                dt += '<td>%s</td></tr>' % (val_show)
                si_total += val_data
            t_text = '<b>Total Children by End of Reporting Month</b>'
            dt += '<tr><td>%s</td>' % (t_text)
            dt += '<td><b>%s</b></td></tr><table>' % (si_total)
            data = dt
    except Exception, e:
        raise e
    else:
        return data


def get_raw_values(params, data_type=1):
    """Method to query summaries."""
    try:
        check_fields = ['org_unit_type_id', 'committee_unit_type_id',
                        'adoption_unit_type_id', 'si_unit_type_id',
                        'cci_unit_type_id', 'ngo_unit_type_id',
                        'government_unit_type_id']
        vals = get_dict(field_name=check_fields)
        orgs = RegOrgUnit.objects.values(
            'org_unit_type_id').annotate(
                unit_count=Count('org_unit_type_id')).order_by('-unit_count')
        dt = '<table class="table table-bordered"><thead>'
        dt += '<tr><th width="40%">Organisation Unit</th>'
        dt += '<th>TOTAL</th></tr></thead>'
        total_ou = 0
        for org in orgs:
            unit_id = org['org_unit_type_id']
            unit_name = vals[unit_id] if unit_id in vals else unit_id
            unit_cnt = org['unit_count']
            total_ou += unit_cnt
            dt += '<tr><td>%s</td>' % (unit_name)
            dt += '<td>%s</td></tr>' % (unit_cnt)
        dt += '<tr><td>Total</td><td>%s</td></tr>' % (total_ou)
        dt += '<table>'
    except Exception, e:
        raise e
    else:
        return dt


if __name__ == '__main__':
    pass
