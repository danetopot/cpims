"""Views for all reports."""
import os
import csv
import urllib
import string
import mimetypes
import calendar
from datetime import datetime
# from reportlab.pdfgen import canvas
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import messages
from django.http import JsonResponse
from .forms import CaseLoad
from .functions import (
    get_case_details, case_load_header, get_data_element,
    simple_document, draw_page, get_geo_locations, get_data, get_period,
    get_sub_county_info, get_raw_data)

from cpovc_registry.models import RegOrgUnit
from cpovc_registry.functions import get_contacts, merge_two_dicts

from openpyxl import load_workbook
from openpyxl.styles.borders import Border, Side

from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas

from django.conf import settings
from django.contrib.auth.decorators import login_required

MEDIA_ROOT = settings.MEDIA_ROOT


@login_required(login_url='/')
def reports_cpims(request, id):
    """Method for all other reports."""
    try:
        doc_id = int(id)
        report_name = 'CPIMS Report'
        form = CaseLoad(request.user, data={'report_id': doc_id})
        docs = {1: 'KNBS Report',
                2: 'NCCS Report',
                3: 'SI and CCI Population Returns Report',
                4: 'Health Report',
                5: 'Ad Hoc Report'}
        if doc_id in docs:
            report_name = docs[doc_id]
        return render(request, 'reports/reports_index.html',
                      {'form': form, 'status': 200, 'doc_id': doc_id,
                       'report_name': report_name})
    except Exception, e:
        raise e


def arrange_pending(mylist):
    """Function to push pending values at end of interventions."""
    my_len = len(mylist)
    val = None
    if my_len > 1:
        for idx, ttt in enumerate(mylist):
            if ttt[2] == 'Pending':
                mylist.remove(ttt)
                # del mylist[idx]
                val = ttt
    vals = mylist
    if val:
        vals.insert(0, val)
    return vals


@login_required(login_url='/')
def reports_home(request):
    """Some default page for reports home page."""
    try:
        address = 'P.O Box %s' % ('.' * 30)
        params, location = {}, '.' * 20
        form = CaseLoad(request.user)
        if request.method == 'POST':
            doc_id = request.POST.get('id')
            doc_name = request.POST.get('name')
            org_unit_id = request.POST.get('org_unit')
            child_id = request.POST.get('child_id')
            file_name = '%s' % (doc_id)
            # Organisation units details
            orgs = RegOrgUnit.objects.select_related().get(
                id=org_unit_id, is_void=False)
            org_contacts = get_contacts(org_unit_id)
            params['org_unit'] = orgs.org_unit_name
            if 'contact_CPOA' in org_contacts:
                address = org_contacts['contact_CPOA'].replace('\r\n', '<br/>')
            params['address'] = address
            # Physical location
            if 'contact_CPHA' in org_contacts:
                location = org_contacts['contact_CPHA'].replace(
                    '\r\n', '<br/>')
            params['physical_location'] = location
            # Get geo details
            geos = get_geo_locations(org_unit_id)
            sub_county = ', '.join(geos)
            params['sub_county'] = sub_county.upper()
            params['child_id'] = int(child_id)
            simple_document(document_name=doc_name, report_name=file_name,
                            params=params)
            results = {'file_name': file_name}
            return JsonResponse(results, content_type='application/json',
                                safe=False)
        return render(request, 'reports/reports_documents.html',
                      {'form': form, 'status': 200})
    except Exception, e:
        raise e


def write_html(data, file_name, report_variables):
    """Method to write html given data."""
    try:
        report_type = report_variables['report_region']
        row_cnt = 0
        table_string = ""
        for val in data:
            row = [str(t) for t in val]
            row_cnt += 1
            table_string += "<tr>" + \
                "<td>" + \
                string.join(row, "</td><td>") + \
                "</td>" + \
                "</tr>\n"
        html = '<table class="table table-bordered">'
        category_title = case_load_header(report_type=report_type, header=True)
        category_title = category_title.format(**report_variables)
        html += '%s%s' % (category_title, table_string)
        html += '</table>'
        return html
    except Exception, e:
        raise e


def write_xlsx(data, file_name, params):
    """Method to write xls given data."""
    try:
        report_type = params['report_region']
        # Define some style for borders
        border = Border(left=Side(style='thin'),
                        right=Side(style='thin'),
                        top=Side(style='thin'),
                        bottom=Side(style='thin'))
        sheet_name = params['sheet'] if 'sheet' in params else 'Sheet'
        xls_tmp = '_orgs' if report_type == 4 else ''
        wb = load_workbook('%s/case_load%s.xltm' % (MEDIA_ROOT, xls_tmp))
        ws = wb.active
        # Lets write some data to the file
        for i, value in enumerate(data):
            for c, stats in enumerate(value):
                ws.cell(row=i + 9, column=c + 1).value = stats
                ws.cell(row=i + 9, column=c + 1).border = border
        # Fill my placeholders with actual parameters
        for idx, row in enumerate(ws.iter_rows('A2:P5')):
            for cell in row:
                if cell.value and "{" in cell.value:
                    cell.value = cell.value.format(**params)
        ws.title = sheet_name
        xls_name = '%s/%s.xlsx' % (MEDIA_ROOT, file_name)
        wb.save(xls_name)
    except Exception, e:
        raise e


def write_csv(data, file_name, params):
    """Method to write csv given data."""
    try:
        with open('%s/%s.csv' % (MEDIA_ROOT, file_name), 'wb') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"',
                                   quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerows(data)
    except Exception, e:
        raise e


def write_pdf(data, file_name):
    """Method to write pdf given data."""
    try:
        pdf_name = '%s/%s.pdf' % (MEDIA_ROOT, file_name)
        doc = SimpleDocTemplate(
            pdf_name, pagesize=A4, rightMargin=30,
            leftMargin=30, topMargin=30, bottomMargin=18)
        doc.pagesize = landscape(A4)
        elements = []
        # TODO: Get this line right instead of just copying it from the
        # docs
        style = TableStyle(
            [('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
             ('BOX', (0, 0), (-1, -1), 0.5, colors.black),
             ('VALIGN', (0, 0), (-1, 0), 'MIDDLE'),
             ('BACKGROUND', (0, 0), (-1, 0), colors.gray)])
        # Configure style and word wrap
        s = getSampleStyleSheet()
        s = s["BodyText"]
        s.wordWrap = 'CJK'
        data2 = [[Paragraph(cell, s) for cell in row] for row in data]
        t = Table(data2)
        t.setStyle(style)
        # Send the data and build the file
        elements.append(t)
        doc.build(elements, onFirstPage=draw_page, onLaterPages=draw_page)

    except Exception, e:
        raise e


def get_totals(all_data, categories, summ=False):
    """Method to get totals for case load."""
    data, cat = [], None
    cases, children = {}, {}
    try:
        if not summ:
            if 'CASE' in all_data:
                cases = all_data['CASE']
                del all_data['CASE']
            if 'CHILD' in all_data:
                children = all_data['CHILD']
                del all_data['CHILD']
        for dt in all_data:
            vls = all_data[dt]
            if 'cat' in vls:
                cat = vls['cat']
                del vls['cat']
            c_data = []
            sc, mc, fc = 0, 0, 0
            for vl in vls:
                sc += 1
                col_val = vls[vl]
                if dt == summ:
                    col_val = len(list(set(vls[vl])))
                c_data.append(col_val)
                if sc % 2:
                    mc += col_val
                else:
                    fc += col_val
            totals = [mc, fc, mc + fc]
            if dt == summ:
                vals = c_data + totals
                data.append(vals)
            else:
                if cat and '_' in cat:
                    dt, cat = cat.split('_')
                if '_' in dt:
                    dt, cat = dt.split('_')
                case_name = categories[dt]
                cat_name = categories[cat] if cat else ''
                vals = [case_name, cat_name] + c_data + totals
                data.append(vals)
        # Return the values
        if not summ:
            if cases:
                all_data['CASE'] = cases
            if children:
                all_data['CHILD'] = children
    except Exception, e:
        print 'error - %s' % (str(e))
        pass
    else:
        return data


@login_required(login_url='/')
def reports_caseload(request):
    """Case load views."""
    results, html = {}, None
    try:
        form = CaseLoad(request.user)
        dates = {v: k for k, v in enumerate(calendar.month_abbr)}
        if request.method == 'POST':
            sub_county_ids = request.POST.getlist('sub_county')
            sub_counties = request.POST.get('sub_county')
            county = request.POST.get('county')
            if not sub_county_ids:
                sub_county_ids = [sub_counties]
            report_type = request.POST.get('report_type')
            rperiod = request.POST.get('report_period')
            report_year = request.POST.get('report_year')
            report_region = int(request.POST.get('report_region'))
            report_unit = request.POST.get('org_unit')
            org_unit_name = request.POST.get('org_unit_name')
            results = {'res': sub_county_ids}
            case_categories = get_case_details(
                ['case_category_id', 'core_item_id', 'intervention_id'])
            categories = {}
            for case_category in case_categories:
                case_id = case_category.item_id
                case_name = case_category.item_description
                categories[case_id] = case_name
            my_county = county if int(report_region) == 2 else False
            if report_region == 1 or report_region == 4:
                sub_county_ids = []
            sub_counties = get_sub_county_info(
                sub_county_ids, icounty=my_county)
            variables = {'sub_county_id': [], 'sub_county': []}
            for sub_county in sub_counties:
                rep_var = sub_counties[sub_county]
                variables['county'] = rep_var['county']
                variables['sub_county_id'].append(rep_var['sub_county_id'])
                variables['sub_county'].append(rep_var['sub_county'])
            # Report variables
            if report_region == 1:
                variables = {'county': 'National', 'sub_county': ['National']}
            variables['sub_county'] = ', '.join(variables['sub_county'])
            today = datetime.now()
            # year = today.strftime('%Y')
            month = today.strftime('%m')
            # Other parameters
            if report_region == 4:
                rep_unit = report_unit if report_unit else False
                variables['org_unit'] = rep_unit
            else:
                variables['org_unit'] = False
            rpd = rperiod[:3] if report_type == 'M' else rperiod
            year = int(report_year) + 1 if report_type == 'M' else report_year
            if report_type == 'Q':
                report_type = rperiod.replace('tr', '')
            # Month value
            month = dates[rpd] if rpd in dates else ''
            period_params = get_period(
                report_type=report_type, year=year, month=month)
            report_variables = merge_two_dicts(variables, period_params)

            all_datas = get_data(report_variables)
            all_data = all_datas['data']
            all_itvs = all_datas['itv']
            all_pending = all_datas['pending']
            report_region_name = report_variables['sub_county']
            rregion = int(report_region)
            if rregion == 2:
                report_region_name = report_variables['county']
            elif rregion == 1:
                report_region_name = 'National'
            elif rregion == 4:
                report_region_name = 'Organisation'
            if not all_data:
                results = {'status': 9, 'file_name': '',
                           'message': 'No data matching your query.'}
                return JsonResponse(results, content_type='application/json',
                                    safe=False)
            file_name = '%s_%s_%s' % (report_region_name,
                                      report_variables['label'],
                                      report_variables['year'])
            report_variables['org_unit_name'] = org_unit_name
            report_variables['org_units_name'] = org_unit_name
            report_variables['report_region'] = report_region
            # Prepare the data
            data = get_totals(all_data, categories)
            data_itv = get_totals(all_itvs, categories)
            data_pend = get_totals(all_pending, categories)
            data.sort()
            cnt = 0
            itvs = {}
            for dsorted in data:
                cnt += 1
                dsorted.insert(0, cnt)
            for kl, dt in enumerate(data):
                dval = dt[1]
                bn = 0
                # get_interventions(data_pend, dval, bn, kl, itvs)
                for ki, itv_data in enumerate(data_itv):
                    itv_cat = itv_data[0]
                    if dval == itv_cat:
                        dts = itv_data
                        bn += 1
                        dts[0] = ''
                        dts.insert(1, '')
                    else:
                        dts = None
                    if dts:
                        # knt = '%s_%s' % (kl + 1, bn)
                        knt = kl + 1
                        if knt not in itvs:
                            itvs[knt] = [dts]
                        else:
                            itvs[knt].append(dts)
                get_interventions(data_pend, dval, bn, kl, itvs)
            fitvs = list(reversed(sorted(itvs.keys())))
            len(data)
            for idx, ttt in enumerate(fitvs):
                ffitvs = itvs[ttt]
                ffitvss = arrange_pending(ffitvs)
                for ffitv in ffitvss:
                    data.insert(ttt, ffitv)
            # Add summaries
            case_data = get_data_element()
            blank = [''] * 16
            data.append(blank)
            sum_vals = get_caseload_summary(all_datas, categories)
            for d_item in case_data:
                cval = case_data[d_item]
                d_items = sum_vals[d_item] if d_item in sum_vals else [0] * 13
                sum_val = ['', cval, ''] + d_items
                data.append(sum_val)
            # Write the data to csv
            write_csv(data, file_name, report_variables)
            # Now write html
            html = write_html(data, file_name, report_variables)
            # Lets write to pdf
            # write_pdf(data, file_name)
            # This is for xlsx
            write_xlsx(data, file_name, report_variables)
            # Output some variables for the report
            results = {'status': 0, 'file_name': file_name, 'report': html,
                       'message': 'No data matching your query.'}
            return JsonResponse(results, content_type='application/json',
                                safe=False)
        return render(request, 'reports/case_load.html',
                      {'form': form, 'results': results,
                       'report': html})
    except Exception, e:
        print 'Case load report error - %s' % (str(e))
        raise e


def get_interventions(data_itv, dval, bn, kl, itvs):
    """Method to get interventions and pending."""
    try:
        for ki, itv_data in enumerate(data_itv):
            itv_cat = itv_data[0]
            if dval == itv_cat:
                dts = itv_data
                bn += 1
                dts[0] = ''
                dts.insert(1, '')
                dts[2] = 'Pending'
            else:
                dts = None
            if dts:
                # knt = '%s_%s' % (kl + 1, bn)
                # itvs[knt] = dts
                knt = kl + 1
                if knt not in itvs:
                    itvs[knt] = [dts]
                else:
                    itvs[knt].append(dts)
        # return itvs
    except Exception, e:
        print 'Error getting intervention', str(e)
        pass


def get_caseload_summary(all_datas, categories):
    """Method to get all case load summary."""
    try:
        vals = {1: 'CHILD', 2: 'CASE', 3: 'INTV', 6: 'PEND'}
        data_keys = {1: 'data', 2: 'data', 3: 'interventions', 6: 'pending'}
        sum_vals = {}
        for val in vals:
            val_name = vals[val]
            key_name = data_keys[val]
            all_sdata = all_datas[key_name]
            if val > 1:
                val_name = 'CASE'
            if val_name in all_sdata:
                all_data = all_sdata[val_name]
                if all_data:
                    key_data = {val_name: all_data}
                    summs = get_totals(key_data, categories, val_name)
                    sum_vals[val] = summs[0]
        return sum_vals
    except Exception, e:
        error = 'Error getting summary - %s' % (str(e))
        print error
        return {}


@login_required(login_url='/')
def reports_generate(request):
    """Case load views."""
    results, html = {}, None
    try:
        dates = {v: k for k, v in enumerate(calendar.month_abbr)}
        if request.method == 'POST':
            sub_county_ids = request.POST.getlist('sub_county[]')
            sub_counties = request.POST.get('sub_county')
            county = request.POST.get('county')
            if not sub_county_ids:
                sub_county_ids = [sub_counties]
            report_type = request.POST.get('report_type')
            rperiod = request.POST.get('report_period')
            report_year = request.POST.get('report_year')
            report_id = request.POST.get('report_id')
            report_region = request.POST.get('report_region')
            results = {'res': sub_county_ids}
            case_categories = get_case_details(['case_category_id'])
            categories = {}

            print 'ID', report_id
            for case_category in case_categories:
                case_id = case_category.item_id
                case_name = case_category.item_description
                categories[case_id] = case_name
            my_county = county if int(report_region) == 2 else False
            if int(report_region) == 1:
                sub_county_ids = []
            sub_counties = get_sub_county_info(
                sub_county_ids, icounty=my_county)
            variables = {'sub_county_id': [], 'sub_county': []}
            for sub_county in sub_counties:
                rep_var = sub_counties[sub_county]
                variables['county'] = rep_var['county']
                variables['sub_county_id'].append(rep_var['sub_county_id'])
                variables['sub_county'].append(rep_var['sub_county'])
            # Report variables
            if int(report_region) == 1:
                variables = {'county': 'National', 'sub_county': ['National']}
            variables['sub_county'] = ', '.join(variables['sub_county'])
            # Report variables
            today = datetime.now()
            # year = today.strftime('%Y')
            month = today.strftime('%m')
            # Other parameters
            variables['report_id'] = report_id
            rpd = rperiod[:3] if report_type == 'M' else rperiod
            ryr = rperiod[0] if report_type == 'Y' else 'F'
            if int(report_id) in [1]:
                ryr = 'C'
            year = int(report_year) + 1 if report_type == 'M' else report_year
            if report_type == 'Q':
                report_type = rperiod.replace('tr', '')
            # Month value
            month = dates[rpd] if rpd in dates else ''
            period_params = get_period(
                report_type=report_type, year=year, month=month, period=ryr)
            report_variables = merge_two_dicts(variables, period_params)
            print 'Newton', report_variables

            # all_datas = get_data(report_variables)
            # all_data = all_datas['data']
            all_data = get_raw_data(report_variables)
            print 'data found', all_data
            if not all_data:
                results = {'status': 9, 'file_name': '',
                           'message': 'No data matching your query.'}
                return JsonResponse(results, content_type='application/json',
                                    safe=False)
            file_name = '%s_%s_%s' % (report_variables['sub_county'],
                                      report_variables['label'],
                                      report_variables['year'])
            # Prepare the data
            html = all_data
            results = {'status': 0, 'file_name': file_name, 'report': html,
                       'message': 'No data matching your query.'}
            return JsonResponse(results, content_type='application/json',
                                safe=False)
        else:
            results = {'status': 0, 'file_name': None, 'report': html,
                       'message': 'Invalid request.'}
            return JsonResponse(results, content_type='application/json',
                                safe=False)
    except Exception, e:
        print 'Error generating report - %s' % (str(e))
        raise e


@login_required(login_url='/')
def reports_download(request, file_name):
    """Generic method for downloading files."""
    try:
        file_path = '%s/%s' % (MEDIA_ROOT, file_name)
        fp = open(file_path, 'rb')
        response = HttpResponse(fp.read())
        fp.close()
        mime_type, encoding = mimetypes.guess_type(file_name)
        if mime_type is None:
            mime_type = 'application/octet-stream'
        response['Content-Type'] = mime_type
        response['Content-Length'] = str(os.stat(file_path).st_size)
        if encoding is not None:
            response['Content-Encoding'] = encoding

        # To inspect details for the below code, see
        # http://greenbytes.de/tech/tc2231/
        if u'WebKit' in request.META['HTTP_USER_AGENT']:
            # Safari 3.0 and Chrome 2.0 accepts UTF-8 encoded string
            # directly.
            filename_header = 'filename=%s' % file_name.encode(
                'utf-8')
        elif u'MSIE' in request.META['HTTP_USER_AGENT']:
            # IE does not support internationalized filename at all.
            # It can only recognize internationalized URL, so we do the
            # trick via routing rules.
            filename_header = ''
        else:
            # For others like Firefox, we follow RFC2231 (encoding
            # extension in HTTP headers).
            filename_header = 'filename*=UTF-8\'\'%s' % urllib.quote(
                file_name.encode('utf-8'))
        response['Content-Disposition'] = 'attachment; ' + filename_header
    except Exception, e:
        msg = 'Error getting file - %s' % (str(e))
        messages.info(request, msg)
        results, html, file_name = {}, None, None
        form = CaseLoad(request.user)
        return render(request, 'reports/case_load.html',
                      {'form': form, 'results': results,
                       'report': html, 'file_name': file_name})
    else:
        return response


@login_required(login_url='/')
def print_pdf(request):
    """Download without printing."""
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="resume.pdf"'
    p = canvas.Canvas(response)

    p.drawString(100, 100, "Some text in first page.")
    p.showPage()

    p.drawString(200, 100, "Some text in second page.")
    p.showPage()

    p.save()
    return response
